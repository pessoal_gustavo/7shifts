$(document).ready(function(){

    $("#tableSpace").fadeOut(1);

    var myColors = ["#33414E","#8DCA35","#00BFDD","#FF702A","#DA3610",
                    "#80CDC2","#A6D969","#D9EF8B","#FFFF99","#F7EC37","#F46D43",
                    "#E08215","#D73026","#A12235","#8C510A","#14514B","#4D9220",
                    "#542688", "#4575B4", "#74ACD1", "#B8E1DE", "#FEE0B6","#FDB863",                                                
                    "#C51B7D","#DE77AE","#EDD3F2"];
    d3.scale.myColors = function() {
        return d3.scale.ordinal().range(myColors);
    };

    var dataChart = [];
    
    table = $("#tblUsers").DataTable();
    table.on('page.dt', function () {
        onresize(100);
    });

    var count = 0;

    var baseUrl = "https://shiftstestapi.firebaseio.com/";
    var locations = null;
    var users = null;
    var timePunches = null;

    var dataProcessed = [];
    
    loadLocations();

    function loadLocations() {
        /**
         * Getting locations
         */
        $.ajax({
            url: baseUrl + "locations.json",
            method: "GET",
            dataType: 'json',
            success: function (result) {
                locations = result;
                console.warn(locations);
                loadUsers();
            },
            error: function (data) {
                alert(data);
            }
        });
    }
    

    function loadUsers() {
        /**
         * Getting users
         */
        $.ajax({
            url: baseUrl + "users.json",
            method: "GET",
            dataType: 'json',
            success: function (result) {
                users = result;
                console.warn(users);
                loadTimePunches();
            },
            error: function (data) {
                alert(data);
            }
        });
    }

    function loadTimePunches() {
        /**
         * Getting time punches
         */
        $.ajax({
            url: baseUrl + "timePunches.json",
            method: "GET",
            dataType: 'json',
            success: function (result) {
                timePunches = result;
                console.warn(timePunches);
                // $("#pleaseWait").hide();
                process();
            },
            error: function (data) {
                alert(data);
            }
        });
    }

    /**
     * NOT ANSWERED QUESTIONS:
     * 
     * 1) If Martina works 48 hours in a week (8 hours Monday - Saturday) she is paid 40 hours normal rate and 8 hours overtime pay rate.
     * BUT, HOW CAN I CALCULATE IF THE SITUATION IS IT:
     * 
     * If Martina works 48 hours in a week (16 hours Monday, 8 hours Wednesday - Saturday) she is paid 40 hours normal rate and 8 hours overtime pay rate.
     * In this case we have dailyovertime (monday) AND weeklyovertime (48 hours)
     */
    function process() {
        $("#pleaseWaitMessage").html("Prcessing data...");

        if (users) {
            /**
             * Itereting the group locations
             */
            Object.keys(users).forEach(function(locationInUseKey,index) {
                console.warn("LOCATION: ", locationInUseKey);
                var locationInUse = locations[locationInUseKey];
                console.warn("LOCATION IN USE: ", locationInUse);
                /**
                 * Iterating the users of this location
                 */
                Object.keys(users[locationInUseKey]).forEach(function(userInUseKey,index) {
                    console.warn("USER: ", userInUseKey);
                    var userInUse = users[locationInUseKey][userInUseKey];
                    console.warn("USER IN USE: ", userInUse);

                    if (userInUse.active) {
                        var dataProcessedItem = {id: userInUse.id, photo: userInUse.photo, firstName: userInUse.firstName, lastName: userInUse.lastName, minutes: 0, minutesWithFactor: 0, dailyOvertime: 0, weeklyOvertime: 0, salary: 0};
                        var dayMinutes = 0;
                        var dayMinutesAcumulative = 0;
                        var weekMinutes = 0;
                        var dayOvertime = 0;
                        /**
                         * Iterating the time punches
                         */
                        Object.keys(timePunches).forEach(function(timePuncheKey,index) {
                            //console.warn("TIME PUNCHE: ", timePuncheKey); //DO NOT DO THIS BECAUSE FREEZE THE BROWSER
                            if (timePunches[timePuncheKey].userId == userInUseKey) {
                                //FINDED THE PUNCHE
                                var clockedIn = timePunches[timePuncheKey].clockedIn;
                                var clockedOut = timePunches[timePuncheKey].clockedOut;
                                console.warn("CLOCKED IN: ", clockedIn);

                                //MAKE THE CALC!!!
                                /**
                                 * First: Is necessary identify if the clocked in and clocked out was in the same day
                                 */
                                if (clockedIn.substring(0, 10) == clockedOut.substring(0, 10)){
                                    //THE SAME DAY
                                    var dCLockedIn = new Date(clockedIn);
                                    var dCLockedOut = new Date(clockedOut);
                                    //Minutes worked in this day
                                    dayMinutes = (((dCLockedOut - dCLockedIn) / 1000)/60);
                                    //Increment in the main object
                                    dataProcessedItem.minutes += dayMinutes;

                                    //Identify the date in week
                                    var weekday = dCLockedIn.getDay();

                                    if (weekday == 0) {
                                        //Sunday, clear the week overtime and week hours
                                        weekMinutes = 0;
                                        dayMinutesAcumulative = 0;
                                    }

                                    //Incrementing week minutes
                                    weekMinutes += dayMinutes;
                                    dayMinutesAcumulative += dayMinutes;

                                    if (weekday == 0 || weekday == 6) {
                                        //Sunday and saturday. Need to be applied special factor for the location
                                        if (dayMinutes > locationInUse.labourSettings.dailyOvertimeThreshold) {
                                            //OVERTIME DAY
                                            
                                            dayOvertime += dayMinutes - locationInUse.labourSettings.dailyOvertimeThreshold;
                                            dataProcessedItem.dailyOvertime = dayOvertime;
                                            dataProcessedItem.minutesWithFactor += (dayMinutes - locationInUse.labourSettings.dailyOvertimeThreshold) * locationInUse.labourSettings.weeklyOvertimeMultiplier;
                                        }
                                    } else {
                                        //monday - friday
                                        if (dayMinutes > locationInUse.labourSettings.dailyOvertimeThreshold) {
                                            //OVERTIME DAY
                                            dayOvertime += dayMinutes - locationInUse.labourSettings.dailyOvertimeThreshold;
                                            dataProcessedItem.dailyOvertime = dayOvertime;
                                            dataProcessedItem.minutesWithFactor += (dayMinutes - locationInUse.labourSettings.dailyOvertimeThreshold) * locationInUse.labourSettings.dailyOvertimeMultiplier;
                                        }
                                    }

                                    if (weekday == 6) {
                                        //Saturday, finish of the week
                                        if (weekMinutes > locationInUse.labourSettings.weeklyOvertimeThreshold) {
                                            //OVERTIME WEEK
                                            dataProcessedItem.weeklyOvertime += weekMinutes - locationInUse.labourSettings.weeklyOvertimeThreshold;

                                            /**
                                             * Imagine that I work 10 hours per day (2 hours in overtime) in monday - friday, I work 50 hours per week (10 overtime)
                                             * I need to calc the diff because the daily overtime is inside the weekly overtime. If I don't calc the diff, I will pay
                                             * extra twice.
                                             */
                                            var diff = weekMinutes - dayMinutesAcumulative;
                                            if (diff > 0) {
                                                dataProcessedItem.minutesWithFactor = (diff - locationInUse.labourSettings.weeklyOvertimeThreshold) * locationInUse.labourSettings.dailyOvertimeMultiplier
                                            }
                                        }
                                    }

                                } else {
                                    //DIFERENT DAY

                                    /**
                                     * 
                                     * NOT IMPLEMENTED YET
                                     * We need to separeted overtime minutes worked in weekend to another
                                     */
                                }
                            }
                        });
                        dataProcessedItem.salary = dataProcessedItem.minutesWithFactor * userInUse.hourlyWage;
                        dataProcessed.push(dataProcessedItem);
                        dataChart.push({ label: dataProcessedItem.firstName, value: dataProcessedItem.salary });
                        table.row.add(['<img width=30 src="' + dataProcessedItem.photo + '" />', dataProcessedItem.firstName, dataProcessedItem.lastName, Math.round(dataProcessedItem.minutes / 60), Math.round(dataProcessedItem.dailyOvertime / 60), Math.round(dataProcessedItem.weeklyOvertime / 60), dataProcessedItem.salary]).draw();
                    } 
                });
            })

            $("#pleaseWait").hide();
            $("#tableSpace").fadeIn(1000);
            
            startChart();

            page_content_onresize();
            console.warn("LINES PROCESSED: ", dataProcessed);
            console.warn("LINES PROCESSED: ", dataProcessed.length);
        } else {
            alert("Users is not defined");
        }
    }

    var startChart = function() {

        console.warn("DATA CHART: ", dataChart);

		nv.addGraph(function() {
			var chart = nv.models.discreteBarChart().x(function(d) {
				return d.label;
			})//Specify the data accessors.
			.y(function(d) {
				return d.value;
			}).staggerLabels(true)//Too many bars and not enough room? Try staggering labels.
			.tooltips(false)//Don't show tooltips
			.showValues(true)//...instead, show the bar value right on top of each bar.
			.transitionDuration(350)
                        .color(d3.scale.myColors().range());;

			d3.select('#chart-4 svg').datum(exampleData()).call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});

		//Each bar represents a single discrete quantity.
		function exampleData() {

			return [{
				key : "Salary",
				values : dataChart
			}];

		}

	};
});